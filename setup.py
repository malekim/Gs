import tkinter as tk
from tkinter import filedialog
import os

application_window = tk.Tk()


# Ask the user to select a folder.
answer = filedialog.askdirectory(parent=application_window,
                                 initialdir=os.getcwd(),
                                 title="Please select a folder:")